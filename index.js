

const http = require('http');

let port = 3000;


const server = http.createServer((req,res)=>{
    if(req.url == '/login'){
        res.writeHead(200, {'Content-Type':'text/plain'});
        res.end('Welcome to Login Page');
    }
  
    else{
        res.writeHead(400, {'Content-Type':'text/plain'});
        res.end(`I'm sorry the page you are looking for cannot be found.`);
    }
})


server.listen(port);


console.log(`Server now running at localhost:${port}`);
